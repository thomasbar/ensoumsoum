#!/usr/bin/env python3

from baratineur import api


if __name__ == "__main__":
    api.run(debug=True)
